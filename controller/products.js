//[SECTION] Dependencies and Modules
   const Product = require('../models/Product')  
   const bcrypt = require('bcrypt');
   const dotenv = require('dotenv');

//[SECTION]Environment variable Setup
	dotenv.config();
	const asin = Number(process.env.SALT);
	

//[SECTION] Functionalities [CREATE]
	//1. Register New Account
	 module.exports.register = (productData) => {
		
		let name = productData.name;
		let description  = productData.description;
		let isActive = productData.isActive;
		let createdOn = productData.createdOn;
	
		
		
		let newProduct = new Product({
			name: name,
			description: description,
			isActive: isActive,
			createdOn: createdOn,
		});
		
		return newProduct.save().then((product, err) =>{
			
			if (product) {
				return product;
			} else {
				return 'Failed to Register product';
			};
		});

	};


