//[SECTION] Dependencies and Modules
   const User = require('../models/User')  
   const bcrypt = require('bcrypt');
   const dotenv = require('dotenv');

//[SECTION]Environment variable Setup
	dotenv.config();
	const asin = Number(process.env.SALT);
	

//[SECTION] Functionalities [CREATE]
	//1. Register New Account
	 module.exports.register = (userData) => {
		
		let email = userData.email;
		let passW  = userData.password;
	
		
		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(passW, asin),
			
		});
		
		return newUser.save().then((user, err) =>{
			
			if (user) {
				return user;
			} else {
				return 'Error';
			};
		});

	};


