//[SECTION] Dependencies and Modules
	const exp = require('express');   
	const controller = require('../controller/products'); 

//[SECTION] Routing Component
	
	const route = exp.Router();

//[SECTION]	Routes-POST
	route.post('/register', (req, res) =>{
		console.log(req.body);
		let productData = req.body;
		controller.register(productData).then(outcome =>{
			res.send(outcome);
		});	
	});

	module.exports = route;

