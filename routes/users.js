//[SECTION] Dependencies and Modules
	const exp = require('express');   
	const controller = require('../controller/users'); 

//[SECTION] Routing Component
	
	const route = exp.Router();

//[SECTION]	Routes-POST
	route.post('/register', (req, res) =>{
		console.log(req.body);
		let userData = req.body;
		controller.register(userData).then(outcome =>{
			res.send(outcome);
		});	
	});

	module.exports = route;

